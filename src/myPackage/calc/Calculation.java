package myPackage.calc;

public class Calculation {

	protected int value = 0;

	protected int result = 0;
	 
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
    public void output() {
        System.out.println(this.result);
    }

}
