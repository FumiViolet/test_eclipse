package myPackage.calc;

public class Square extends Calculation {

    public void calculate() {
        // valueを2乗する
        this.result = this.value * this.value;

        // 結果を出力する
        this.output();
    }
	
}
